// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "app_name" : MessageLookupByLibrary.simpleMessage("photo"),
    "browse_all" : MessageLookupByLibrary.simpleMessage("Browse All"),
    "discover" : MessageLookupByLibrary.simpleMessage("Discover"),
    "email" : MessageLookupByLibrary.simpleMessage("Email"),
    "log_in" : MessageLookupByLibrary.simpleMessage("Log in"),
    "password" : MessageLookupByLibrary.simpleMessage("Password"),
    "register" : MessageLookupByLibrary.simpleMessage("Register"),
    "see_more" : MessageLookupByLibrary.simpleMessage("See More"),
    "sign_up" : MessageLookupByLibrary.simpleMessage("Sign up"),
    "sign_up_description" : MessageLookupByLibrary.simpleMessage("By signing up, you agree to Photo’s <a href=\"terms_and_service_link\">Terms of Service</a> and <a href=\"privacy_policy_link\">Privacy Policy.</a>"),
    "whats_new_today" : MessageLookupByLibrary.simpleMessage("What\'s New Today")
  };
}
