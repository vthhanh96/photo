// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a vi_VN locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'vi_VN';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "app_name" : MessageLookupByLibrary.simpleMessage("photo"),
    "browse_all" : MessageLookupByLibrary.simpleMessage("Tất cả"),
    "discover" : MessageLookupByLibrary.simpleMessage("Khám phá"),
    "email" : MessageLookupByLibrary.simpleMessage("Email"),
    "log_in" : MessageLookupByLibrary.simpleMessage("Đăng nhập"),
    "password" : MessageLookupByLibrary.simpleMessage("Mật khẩu"),
    "register" : MessageLookupByLibrary.simpleMessage("Đăng ký"),
    "see_more" : MessageLookupByLibrary.simpleMessage("Xem thêm"),
    "sign_up" : MessageLookupByLibrary.simpleMessage("Đăng ký"),
    "sign_up_description" : MessageLookupByLibrary.simpleMessage("Khi đăng ký, bạn đồng ý với <a href=\"terms_and_service_link\">Điều khoản dịch vụ</a> and <a href=\"privacy_policy_link\">Chính sách bảo mật.</a> của Photo."),
    "whats_new_today" : MessageLookupByLibrary.simpleMessage("Hôm nay có gì mới")
  };
}
