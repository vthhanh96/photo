import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'injector.config.dart';

final injector = GetIt.instance;

@injectableInit
Future<GetIt> setupInjector() => $initGetIt(injector, environment: Environment.dev);

@module
abstract class RegisterModule {
  @lazySingleton
  @preResolve
  Future<SharedPreferences> getSharePreferences() async => SharedPreferences.getInstance();

  @lazySingleton
  FirebaseAuth getFirebaseAuth() => FirebaseAuth.instance;
}