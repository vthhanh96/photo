import 'package:injectable/injectable.dart';
import 'package:photo/assets.gen.dart';
import 'package:photo/modules/auth/domain/models/account.dart';
import 'package:photo/modules/main/domain/models/photo.dart';

abstract class PhotoDataSource {
  Future<Photo> getNewPhoto();

  Future<List<Photo>> getPhotos(int page);
}

@LazySingleton(as: PhotoDataSource)
class PhotoDataSouceFake extends PhotoDataSource {

  final account = Account(name: "Ridhwan Nordin", cashTag: "ridjzcob", photoURL: Assets.resources.icons.icAvatar.path);

  @override
  Future<Photo> getNewPhoto() {
    return Future.value(Photo(Assets.resources.icons.imgNews.path, account));
  }

  @override
  Future<List<Photo>> getPhotos(int page) {
    return Future.value([
      Photo(Assets.resources.icons.img1.path, account),
      Photo(Assets.resources.icons.img2.path, account),
      Photo(Assets.resources.icons.img3.path, account),
      Photo(Assets.resources.icons.img4.path, account),
      Photo(Assets.resources.icons.img5.path, account),
      Photo(Assets.resources.icons.img6.path, account),
      Photo(Assets.resources.icons.img7.path, account),
      Photo(Assets.resources.icons.img8.path, account),
      Photo(Assets.resources.icons.img9.path, account),
      Photo(Assets.resources.icons.img10.path, account),
    ]);
  }

}