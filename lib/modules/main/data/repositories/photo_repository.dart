import 'package:injectable/injectable.dart';
import 'package:photo/modules/main/data/datasources/photo_datasource.dart';
import 'package:photo/modules/main/domain/models/photo.dart';

@lazySingleton
class PhotoRepository {

  final PhotoDataSource _photoDataSource;

  PhotoRepository(this._photoDataSource);

  Future<Photo> getNewPhoto() {
    return _photoDataSource.getNewPhoto();
  }

  Future<List<Photo>> getPhotos(int page) {
    return _photoDataSource.getPhotos(page);
  }
}