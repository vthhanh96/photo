import 'package:injectable/injectable.dart';
import 'package:photo/modules/main/data/repositories/photo_repository.dart';
import 'package:photo/modules/main/domain/models/photo.dart';

@lazySingleton
class GetPhotosUseCase {
  final PhotoRepository _photoRepository;

  GetPhotosUseCase(this._photoRepository);

  Future<List<Photo>> call(int page) {
    return _photoRepository.getPhotos(page);
  }
}