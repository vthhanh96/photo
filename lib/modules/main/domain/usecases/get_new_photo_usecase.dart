import 'package:injectable/injectable.dart';
import 'package:photo/modules/main/data/repositories/photo_repository.dart';
import 'package:photo/modules/main/domain/models/photo.dart';

@lazySingleton
class GetNewPhotoUseCase {
  final PhotoRepository _photoRepository;

  GetNewPhotoUseCase(this._photoRepository);

  Future<Photo> call() {
    return _photoRepository.getNewPhoto();
  }
}