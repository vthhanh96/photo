import 'package:photo/modules/auth/domain/models/account.dart';

class Photo {
  String path;
  Account user;

  Photo(this.path, this.user);
}