import 'package:flutter/material.dart';
import 'package:photo/core/theme/ui_text_style.dart';
import 'package:photo/generated/l10n.dart';
import 'package:photo/modules/main/app/ui/widgets/browse_widget.dart';
import 'package:photo/modules/main/app/ui/widgets/news_widget.dart';

class DiscoverPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).discover,
              style: UITextStyle.black_36_w400_confortaa,
            ),
            const SizedBox(height: 32,),
            NewsWidget(),
            const SizedBox(height: 32,),
            BrowseWidget()
          ],
        ),
      ),
    );
  }
}
