import 'package:flutter/material.dart';

class AddWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 40,
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0, 1],
          colors: [Color(0xFFFF00D6), Color(0xFFFF4D00)]
        ),
        borderRadius: BorderRadius.circular(20),
      ),
      child: const Icon(Icons.add, color: Colors.white,),
    );
  }
}
