import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:photo/core/theme/ui_text_style.dart';
import 'package:photo/core/ui/base_view_state.dart';
import 'package:photo/core/ui/widgets/button_widget.dart';
import 'package:photo/generated/l10n.dart';
import 'package:photo/modules/main/app/ui/widgets/browse_widget_bloc.dart';
import 'package:photo/modules/main/domain/models/photo.dart';

class BrowseWidget extends StatefulWidget {
  @override
  _BrowseWidgetState createState() => _BrowseWidgetState();
}

class _BrowseWidgetState extends BaseViewState<BrowseWidget, BrowseWidgetBloc> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          S.of(context).browse_all.toUpperCase(),
          style: UITextStyle.black_13_w700,
        ),
        const SizedBox(
          height: 16,
        ),
        StreamBuilder<List<Photo>>(
          stream: bloc.getPhotosStream,
          builder: (context, snapshot) {
            final result = snapshot.data;
            if (result == null || result.isEmpty) return const SizedBox();
            return StaggeredGridView.countBuilder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              crossAxisCount: 4,
              itemCount: result.length,
              itemBuilder: (BuildContext context, int index) {
                return Image.asset(result[index].path, fit: BoxFit.cover,);
              },
              staggeredTileBuilder: (index) => StaggeredTile.count(2, index.isEven ? 4 : 3),
              mainAxisSpacing: 8,
              crossAxisSpacing: 8,
            );
          }
        ),
        const SizedBox(
          height: 16,
        ),
        ButtonWidget(
          title: S.of(context).see_more.toUpperCase(),
          onPressed: () => bloc.getPhotos(),
          borderColor: Colors.black,
          cornerRadius: 5,
          titleTextStyle: UITextStyle.black_13_w700,
        )
      ],
    );
  }
}
