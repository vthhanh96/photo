import 'package:flutter/material.dart';
import 'package:photo/assets.gen.dart';
import 'package:photo/core/theme/ui_text_style.dart';
import 'package:photo/modules/auth/domain/models/account.dart';

class UserInfoWidget extends StatelessWidget {
  final Account account;

  const UserInfoWidget({this.account});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          backgroundColor: Colors.white,
          child: Assets.resources.icons.icAvatar
              .image(width: 28, height: 28, fit: BoxFit.fill),
        ),
        const SizedBox(
          width: 16,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              account.name ?? "",
              style: UITextStyle.black_13_w700,
            ),
            Text(
              account.cashTag ?? "",
              style: UITextStyle.black_11_w400,
            )
          ],
        )
      ],
    );
  }
}
