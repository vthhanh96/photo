import 'package:flutter/material.dart';
import 'package:photo/core/theme/ui_text_style.dart';
import 'package:photo/core/ui/base_view_state.dart';
import 'package:photo/generated/l10n.dart';
import 'package:photo/modules/main/app/ui/widgets/news_widget_bloc.dart';
import 'package:photo/modules/main/app/ui/widgets/user_info_widget.dart';
import 'package:photo/modules/main/domain/models/photo.dart';

class NewsWidget extends StatefulWidget {
  @override
  _NewsWidgetState createState() => _NewsWidgetState();
}

class _NewsWidgetState extends BaseViewState<NewsWidget, NewsWidgetBloc> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(S.of(context).whats_new_today.toUpperCase(), style: UITextStyle.black_13_w700,),
        const SizedBox(height: 16,),
        StreamBuilder<Photo>(
          stream: bloc.photoStream,
          builder: (context, snapshot) {
            final photo = snapshot.data;
            if (photo == null) return const SizedBox();
            return AspectRatio(aspectRatio: 1, child: Image.asset(photo.path, fit: BoxFit.cover,));
          }
        ),
        const SizedBox(height: 16,),
        StreamBuilder<Photo>(
          stream: bloc.photoStream,
          builder: (context, snapshot) {
            final account = snapshot.data?.user;
            if (account == null) return const SizedBox();
            return UserInfoWidget(account: account,);
          }
        ),
      ],
    );
  }
}
