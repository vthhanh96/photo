import 'dart:async';

import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:photo/core/ui/base_bloc.dart';
import 'package:photo/modules/main/domain/models/photo.dart';
import 'package:photo/modules/main/domain/usecases/get_photos_usecase.dart';

@injectable
class BrowseWidgetBloc extends BaseBloc {
  final GetPhotosUseCase _getPhotosUseCase;

  BrowseWidgetBloc(this._getPhotosUseCase);

  final _getPhotosStreamController = StreamController<List<Photo>>();

  Stream<List<Photo>> get getPhotosStream => _getPhotosStreamController.stream;

  int page = 1;
  final List<Photo> _photo = [];

  @override
  void initState() {
    super.initState();
    getPhotos();
  }

  Future getPhotos() async {
    try {
      final result = await _getPhotosUseCase(page);
      _photo.addAll(result);
      _getPhotosStreamController.sink.add(_photo);
      page++;
    } catch (error) {
      debugPrint(error.toString());
    }
  }

  @override
  void disposeState() {
    super.disposeState();
    _getPhotosStreamController.close();
  }
}