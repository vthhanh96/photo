import 'dart:async';

import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:photo/core/ui/base_bloc.dart';
import 'package:photo/modules/main/domain/models/photo.dart';
import 'package:photo/modules/main/domain/usecases/get_new_photo_usecase.dart';

@injectable
class NewsWidgetBloc extends BaseBloc {
  final GetNewPhotoUseCase _getNewPhotoUseCase;

  NewsWidgetBloc(this._getNewPhotoUseCase);

  final _getNewPhotoStreamController = StreamController<Photo>.broadcast();

  Stream<Photo> get photoStream => _getNewPhotoStreamController.stream;

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  Future _loadData() async {
    try {
      final result = await _getNewPhotoUseCase();
      _getNewPhotoStreamController.sink.add(result);
    } catch (error) {
      debugPrint(error.toString());
    }
  }

  @override
  void disposeState() {
    super.disposeState();
    _getNewPhotoStreamController.close();
  }
}