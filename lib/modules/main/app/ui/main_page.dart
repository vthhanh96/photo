import 'package:flutter/material.dart';
import 'package:photo/assets.gen.dart';
import 'package:photo/core/ui/base_view_state.dart';
import 'package:photo/modules/main/app/ui/discover_page.dart';
import 'package:photo/modules/main/app/ui/main_page_bloc.dart';
import 'package:photo/modules/main/app/ui/widgets/add_widget.dart';

class MainPage extends StatefulWidget {

  static void start(BuildContext context) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => MainPage()), (_) => false);
  }

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends BaseViewState<MainPage, MainPageBloc> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: DiscoverPage(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: [
          BottomNavigationBarItem(icon: Assets.resources.icons.icHome.image(width: 15, height: 15), label: "Discover"),
          BottomNavigationBarItem(icon: Assets.resources.icons.icSearch.image(width: 15, height: 15), label: "Explore"),
          BottomNavigationBarItem(icon: AddWidget(), label: "Add"),
          BottomNavigationBarItem(icon: Assets.resources.icons.icChat.image(width: 15, height: 15), label: "Chat"),
          BottomNavigationBarItem(icon: Assets.resources.icons.icAccount.image(width: 15, height: 15), label: "Account"),
        ],
      ),
    );
  }
}
