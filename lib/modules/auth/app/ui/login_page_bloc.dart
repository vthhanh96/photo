import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:photo/core/ui/base_bloc.dart';
import 'package:photo/modules/auth/app/ui/events/go_to_main_page_event.dart';
import 'package:photo/modules/auth/app/ui/events/login_error_event.dart';
import 'package:photo/modules/auth/domain/usecases/login_usecase.dart';

@injectable
class LoginPageBloc extends BaseBloc {

  final LoginUseCase _loginUseCase;

  LoginPageBloc(this._loginUseCase);

  final _loadingStreamController = StreamController<bool>();

  Stream<bool> get loadingStream => _loadingStreamController.stream;

  Future login(String email, String password) async {
    try {
      _loadingStreamController.sink.add(true);
      await _loginUseCase(email, password);
      _loadingStreamController.sink.add(false);
      eventSink.add(GoToMainPageEvent());
    } catch (error) {
      eventSink.add(LoginErrorEvent(error.toString()));
      _loadingStreamController.sink.add(false);
    }
  }

  @override
  void disposeState() {
    super.disposeState();
    _loadingStreamController.close();
  }
}