import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:photo/core/ui/base_bloc.dart';
import 'package:photo/modules/auth/app/ui/events/go_to_authentication_page_event.dart';
import 'package:photo/modules/auth/app/ui/events/go_to_main_page_event.dart';
import 'package:photo/modules/auth/domain/usecases/check_logged_in_usecase.dart';

@injectable
class SplashPageBloc extends BaseBloc {

  final CheckLoggedInUseCase _checkLoggedInUseCase;

  SplashPageBloc(this._checkLoggedInUseCase);

  @override
  void initState() {
    super.initState();
    _checkLoggedIn();
  }
  
  Future _checkLoggedIn() async {
    await Future.delayed(const Duration(seconds: 1));
    try {
      final result = await _checkLoggedInUseCase();
      if (result) {
        eventSink.add(GoToMainPageEvent());
      } else {
        eventSink.add(GoToAuthenticationPageEvent());
      }
    } catch (error) {
      debugPrint(error.toString());
    }
  }
}