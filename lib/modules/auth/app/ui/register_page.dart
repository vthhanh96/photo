import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:photo/assets.gen.dart';
import 'package:photo/core/theme/ui_text_style.dart';
import 'package:photo/core/ui/base_view_state.dart';
import 'package:photo/core/ui/widgets/button_widget.dart';
import 'package:photo/core/ui/widgets/default_outline_input_border.dart';
import 'package:photo/generated/l10n.dart';
import 'package:photo/modules/auth/app/ui/register_page_bloc.dart';

class RegisterPage extends StatefulWidget {
  static void start(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => RegisterPage()));
  }

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends BaseViewState<RegisterPage, RegisterPageBloc> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Assets.resources.icons.icBack
              .image(width: 15, height: 15, fit: BoxFit.fill),
          onPressed: () => Navigator.of(context).maybePop(),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).register,
              style: UITextStyle.black_36_w400_confortaa,
            ),
            const SizedBox(
              height: 32,
            ),
            TextField(
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  enabledBorder: const DefaultOutlineInputOrder(),
                  focusedBorder: const DefaultOutlineInputOrder(),
                  hintText: S.of(context).email),
            ),
            const SizedBox(
              height: 16,
            ),
            ButtonWidget(
              title: S.of(context).sign_up.toUpperCase(),
              onPressed: () {},
              backgroundColor: Colors.black,
              borderColor: Colors.black,
              titleTextStyle: UITextStyle.white_13_w700,
              cornerRadius: 5,
            ),
            const SizedBox(
              height: 32,
            ),
            Html(
              data: S.of(context).sign_up_description,
              onLinkTap: (link) {},
              defaultTextStyle: UITextStyle.black_13_w400,
              linkStyle: const TextStyle(
                  decoration: TextDecoration.underline, color: Colors.black),
            )
          ],
        ),
      ),
    );
  }
}
