import 'package:flutter/material.dart';
import 'package:photo/assets.gen.dart';
import 'package:photo/core/theme/ui_text_style.dart';
import 'package:photo/core/ui/widgets/button_widget.dart';
import 'package:photo/generated/l10n.dart';
import 'package:photo/modules/auth/app/ui/login_page.dart';
import 'package:photo/modules/auth/app/ui/register_page.dart';

class AuthenticationPage extends StatelessWidget {

  static void start(BuildContext context) {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => AuthenticationPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Assets.resources.icons.imgBackground.image(
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover),
            Column(
              children: [
                Expanded(
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Assets.resources.icons.icLogo.image(),
                        const SizedBox(
                          width: 16,
                        ),
                        Text(
                          S.of(context).app_name,
                          style: UITextStyle.black_48_w400_confortaa,
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  color: Colors.white,
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    children: [
                      Expanded(child: ButtonWidget(
                        title: S.of(context).log_in.toUpperCase(),
                        onPressed: () => LoginPage.start(context),
                        borderColor: Colors.black,
                        titleTextStyle: UITextStyle.black_13_w700,
                        cornerRadius: 5,)),
                      const SizedBox(width: 16,),
                      Expanded(child: ButtonWidget(
                        title: S.of(context).register.toUpperCase(),
                        onPressed: () => RegisterPage.start(context),
                        borderColor: Colors.black,
                        backgroundColor: Colors.black,
                        titleTextStyle: UITextStyle.white_13_w700,
                        cornerRadius: 5,))
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
