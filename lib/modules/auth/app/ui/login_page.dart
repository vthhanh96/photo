import 'package:flutter/material.dart';
import 'package:photo/assets.gen.dart';
import 'package:photo/core/theme/ui_text_style.dart';
import 'package:photo/core/ui/base_event.dart';
import 'package:photo/core/ui/base_view_state.dart';
import 'package:photo/core/ui/widgets/button_widget.dart';
import 'package:photo/core/ui/widgets/default_outline_input_border.dart';
import 'package:photo/core/ui/widgets/loading_widget.dart';
import 'package:photo/generated/l10n.dart';
import 'package:photo/modules/auth/app/ui/events/go_to_main_page_event.dart';
import 'package:photo/modules/auth/app/ui/events/login_error_event.dart';
import 'package:photo/modules/auth/app/ui/login_page_bloc.dart';
import 'package:photo/modules/main/app/ui/main_page.dart';
import 'package:photo/core/extensions/context_ext.dart';

class LoginPage extends StatefulWidget {
  static void start(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => LoginPage()));
  }

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends BaseViewState<LoginPage, LoginPageBloc> {

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void onBlocEventListener(BaseEvent event) {
    switch (event.runtimeType) {
      case LoginErrorEvent:
        context.toast((event as LoginErrorEvent).errorMessage);
        break;
      case GoToMainPageEvent:
        MainPage.start(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Assets.resources.icons.icBack
              .image(width: 15, height: 15, fit: BoxFit.fill),
          onPressed: () => Navigator.of(context).maybePop(),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  S.of(context).log_in,
                  style: UITextStyle.black_36_w400_confortaa,
                ),
                const SizedBox(
                  height: 32,
                ),
                TextField(
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      enabledBorder: const DefaultOutlineInputOrder(),
                      focusedBorder: const DefaultOutlineInputOrder(),
                      hintText: S.of(context).email),
                ),
                const SizedBox(
                  height: 16,
                ),
                TextField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                      enabledBorder: const DefaultOutlineInputOrder(),
                      focusedBorder: const DefaultOutlineInputOrder(),
                      hintText: S.of(context).password),
                ),
                const SizedBox(
                  height: 16,
                ),
                ButtonWidget(
                  title: S.of(context).log_in.toUpperCase(),
                  onPressed: () {
                    context.hideKeyboard();
                    bloc.login(_emailController.text, _passwordController.text);
                  },
                  backgroundColor: Colors.black,
                  borderColor: Colors.black,
                  titleTextStyle: UITextStyle.white_13_w700,
                  cornerRadius: 5,
                )
              ],
            ),
          ),
          StreamBuilder<bool>(
            stream: bloc.loadingStream,
            builder: (context, snapshot) {
              final result = snapshot.data;
              if (result == null || !result) return const SizedBox();
              return LoadingWidget();
            }
          ),
        ],
      ),
    );
  }
}
