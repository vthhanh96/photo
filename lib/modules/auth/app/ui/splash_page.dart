import 'package:flutter/material.dart';
import 'package:photo/assets.gen.dart';
import 'package:photo/core/theme/ui_text_style.dart';
import 'package:photo/core/ui/base_event.dart';
import 'package:photo/core/ui/base_view_state.dart';
import 'package:photo/generated/l10n.dart';
import 'package:photo/modules/auth/app/ui/authentication_page.dart';
import 'package:photo/modules/auth/app/ui/events/go_to_authentication_page_event.dart';
import 'package:photo/modules/auth/app/ui/events/go_to_main_page_event.dart';
import 'package:photo/modules/auth/app/ui/splash_page_bloc.dart';
import 'package:photo/modules/main/app/ui/main_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends BaseViewState<SplashPage, SplashPageBloc> {

  @override
  void onBlocEventListener(BaseEvent event) {
    switch (event.runtimeType) {
      case GoToAuthenticationPageEvent:
        AuthenticationPage.start(context);
        break;
      case GoToMainPageEvent:
        MainPage.start(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Assets.resources.icons.imgBackground.image(
              width: double.infinity, height: double.infinity, fit: BoxFit.cover),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Assets.resources.icons.icLogo.image(),
                const SizedBox(width: 16,),
                Text(S.of(context).app_name, style: UITextStyle.black_48_w400_confortaa,)
              ],
            ),
          )
        ],
      ),
    );
  }
}
