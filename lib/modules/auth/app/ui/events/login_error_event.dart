import 'package:photo/core/ui/base_event.dart';

class LoginErrorEvent extends BaseEvent {
  final String errorMessage;

  LoginErrorEvent(this.errorMessage);
}