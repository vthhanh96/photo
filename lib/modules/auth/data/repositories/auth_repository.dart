import 'package:injectable/injectable.dart';
import 'package:photo/modules/auth/data/datasources/auth_datasource.dart';
import 'package:photo/modules/auth/domain/models/account.dart';

@LazySingleton()
class AuthRepository {

  final AuthDataSource _authDataSource;

  AuthRepository(this._authDataSource);

  Future<bool> isLoggedIn() {
    return _authDataSource.isLoggedIn();
  }

  Future<Account> login(String email, String password) {
    return _authDataSource.login(email, password);
  }
}