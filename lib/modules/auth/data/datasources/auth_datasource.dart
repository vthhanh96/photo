import 'package:firebase_auth/firebase_auth.dart';
import 'package:injectable/injectable.dart';
import 'package:photo/modules/auth/domain/models/account.dart';

abstract class AuthDataSource {
  Future<Account> login(String email, String password);

  Future<bool> isLoggedIn();
}

@LazySingleton(as: AuthDataSource)
class AuthDataSourceImpl extends AuthDataSource {
  final FirebaseAuth _firebaseAuth;

  AuthDataSourceImpl(this._firebaseAuth);

  @override
  Future<bool> isLoggedIn() {
    return Future.value(_firebaseAuth.currentUser != null);
  }

  @override
  Future<Account> login(String email, String password) async {
    try {
      final UserCredential userCredential = await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);
      return Account(
        email: userCredential.user.email,
        uid: userCredential.user.uid,
        name: userCredential.user.displayName,
        photoURL: userCredential.user.photoURL,
      );
    } on FirebaseAuthException catch (error) {
      switch (error.code) {
        case 'user-not-found':
          throw Exception('No user found for that email');
          break;
        case 'wrong-password':
          throw Exception('Wrong password provided for that user');
          break;
        case 'invalid-email':
          throw Exception('Invalid email');
          break;
        default:
          throw Exception(error.message);
      }
    }
  }
}
