import 'package:injectable/injectable.dart';
import 'package:photo/modules/auth/data/repositories/auth_repository.dart';

@LazySingleton()
class CheckLoggedInUseCase {
  final AuthRepository _authRepository;
  
  CheckLoggedInUseCase(this._authRepository);

  Future<bool> call() {
    return _authRepository.isLoggedIn();
  }
}