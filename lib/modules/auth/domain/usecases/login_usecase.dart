import 'package:injectable/injectable.dart';
import 'package:photo/modules/auth/data/repositories/auth_repository.dart';
import 'package:photo/core/extensions/string_ext.dart';

@LazySingleton()
class LoginUseCase {
  final AuthRepository _authRepository;

  LoginUseCase(this._authRepository);

  Future call(String email, String password) async {
    if (email.isEmpty) {
      throw Exception("Email should not empty");
    }

    if (!email.isValidEmail()) {
      throw Exception("Invalid email");
    }

    if (password.isEmpty) {
      throw Exception("Password should not empty");
    }

    await _authRepository.login(email, password);
  }
}