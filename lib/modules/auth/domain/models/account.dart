class Account {
  String uid;
  String email;
  String photoURL;
  String name;
  String cashTag;
  
  Account({this.uid, this.email, this.photoURL, this.name, this.cashTag});
}