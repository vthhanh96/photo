import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:photo/core/ui/base_event.dart';

abstract class BaseBloc {

  final _eventStreamController = StreamController<BaseEvent>();

  Stream<BaseEvent> get eventStream => _eventStreamController.stream;
  Sink<BaseEvent> get eventSink => _eventStreamController.sink;

  void initState() {}

  @mustCallSuper
  void disposeState() {
    _eventStreamController.close();
  }
}
