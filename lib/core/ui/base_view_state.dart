import 'package:flutter/material.dart';
import 'package:photo/core/ui/base_bloc.dart';
import 'package:photo/core/ui/base_event.dart';
import 'package:photo/injector.dart';

abstract class BaseViewState<T extends StatefulWidget, B extends BaseBloc>
    extends State<T> {
  final B bloc = injector<B>();

  @protected
  void loadArguments() {}

  @protected
  void onBlocEventListener(BaseEvent event) {}

  @override
  void initState() {
    super.initState();
    loadArguments();
    bloc.initState();
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => bloc.eventStream.listen(onBlocEventListener));
  }

  @override
  void didUpdateWidget(covariant T oldWidget) {
    super.didUpdateWidget(oldWidget);
    loadArguments();
  }

  @override
  void dispose() {
    bloc.disposeState();
    super.dispose();
  }
}
