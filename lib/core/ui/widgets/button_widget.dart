import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  final String title;
  final Color backgroundColor;
  final Color borderColor;
  final TextStyle titleTextStyle;
  final double cornerRadius;
  final double height;
  final VoidCallback onPressed;

  const ButtonWidget({
    @required this.title,
    @required this.onPressed,
    this.backgroundColor,
    this.borderColor,
    this.titleTextStyle,
    this.cornerRadius,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: ButtonStyle(
          backgroundColor:
              MaterialStateProperty.all(backgroundColor ?? Colors.white),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(cornerRadius ?? 0))),
          side: MaterialStateProperty.all(BorderSide(
              color: borderColor ?? backgroundColor ?? Colors.white)),
      minimumSize: MaterialStateProperty.all(Size(double.infinity, height ?? 52))),
      child: Text(
        title ?? "",
        style: titleTextStyle,
      ),
    );
  }
}
