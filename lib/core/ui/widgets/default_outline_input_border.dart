import 'package:flutter/material.dart';

class DefaultOutlineInputOrder extends OutlineInputBorder {
  const DefaultOutlineInputOrder() : super(
            borderSide: const BorderSide(width: 2),
            borderRadius: const BorderRadius.all(
              Radius.circular(0),
            ));
}
