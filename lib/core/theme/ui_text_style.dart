import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class UITextStyle {
  static TextStyle black_48_w400_confortaa = GoogleFonts.comfortaa(color: Colors.black, fontSize: 48, fontWeight: FontWeight.normal);
  static TextStyle black_36_w400_confortaa = GoogleFonts.comfortaa(color: Colors.black, fontSize: 36, fontWeight: FontWeight.normal);

  static const TextStyle black_11_w400 = TextStyle(color: Colors.black, fontSize: 11, fontWeight: FontWeight.normal);
  static const TextStyle black_13_w400 = TextStyle(color: Colors.black, fontSize: 13, fontWeight: FontWeight.normal);
  static const TextStyle black_13_w700 = TextStyle(color: Colors.black, fontSize: 13, fontWeight: FontWeight.bold);
  static const TextStyle white_13_w700 = TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.bold);
}