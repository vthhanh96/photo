import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

extension BuildContextExt on BuildContext {
  void toast(String message) {
    Toast.show(message, this, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  void hideKeyboard() {
    final FocusScopeNode currentFocus = FocusScope.of(this);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }
}
